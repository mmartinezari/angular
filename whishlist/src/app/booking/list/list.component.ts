import { Component, OnInit } from '@angular/core';
import { BookinApiClientService } from '../bookin-api-client.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(public api: BookinApiClientService) { }

  ngOnInit(): void {
  }

}

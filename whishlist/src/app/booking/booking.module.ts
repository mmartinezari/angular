import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingRoutingModule } from './booking-routing.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { BookinApiClientService } from './bookin-api-client.service';


@NgModule({
  declarations: [
    ListComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    BookingRoutingModule
  ],
  providers: [
    BookinApiClientService
  ]
})
export class BookingModule { }

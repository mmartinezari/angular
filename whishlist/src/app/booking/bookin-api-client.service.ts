import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookinApiClientService {

  constructor() { }

  getAll() {
    return [{ id: 1, name: 'Uno'}, { id: 2, name: 'Dos' }];
  }
}

import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Trip } from './trip.model'
import { HttpClientModule } from '@angular/common/http'

export interface TripState {
  items: Trip[];
  loading: boolean;
  favorite: Trip;
}

export function initializeTripState() {
  return {
    items: [],
    loading: false,
    favorite: null
  };
}

export enum TripActionType {
  INIT = '[Trip Inicializacion]',
  NEW = '[Trip] Nuevo',
  FAVORITE = '[Trip] Favorito',
  VOTE_UP = '[Trip] Vote Up',
  VOTE_DOWN = '[Trip] Vote Down'
}

export class InitTripAction implements Action {
  type = TripActionType.INIT;
  constructor(public trips: string[]) {}
}

export class NewTripAction implements Action {
  type = TripActionType.NEW;
  constructor(public trip: Trip) {}
}

export class FavoriteTripAction implements Action {
  type = TripActionType.FAVORITE;
  constructor(public trip: Trip) {}
}

export class VoteUpTripAction implements Action {
  type = TripActionType.VOTE_DOWN;
  constructor(public trip: Trip) {}
}

export class VoteDownTripAction implements Action {
  type = TripActionType.VOTE_DOWN;
  constructor(public trip: Trip) {}
}

export type TripActions = InitTripAction | NewTripAction | FavoriteTripAction | VoteUpTripAction | VoteDownTripAction;

export function reducerTrips (state: TripState, action: TripActions) : TripState {
  switch (action.type) {
    case TripActionType.INIT: {
      const trips: string[] = (action as InitTripAction).trips;
      return {
        ...state,
        items: trips.map((t) => new Trip(t))
      }
    }
    case TripActionType.NEW: {
      return {
        ...state,
        items: [...state.items, (action as NewTripAction).trip]
      };
    }
    case TripActionType.FAVORITE: {
      //state.items.forEach(x => x.setSelected(false));
      let trip: Trip = (action as FavoriteTripAction).trip;
      //trip.setSelected(true);
      return {
        ...state,
        favorite: trip
      };
    }
    case TripActionType.VOTE_UP: {
      let trip: Trip = (action as VoteUpTripAction).trip;
      trip.voteUp();
      return { ...state };
    }
    case TripActionType.VOTE_DOWN: {
      let trip: Trip = (action as VoteUpTripAction).trip;
      trip.voteUp();
      return { ...state };
    }
  }
  return state;
}

@Injectable()
export class TripEffects {
  @Effect()
  newItemAdded$: Observable<Action> = this.actions$.pipe(
    ofType(TripActionType.NEW),
    map((action: NewTripAction) => new FavoriteTripAction(action.trip))
  );

  constructor(private actions$: Actions) {}
}

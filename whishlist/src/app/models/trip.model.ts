import {v4 as uuid} from 'uuid';

export class Trip {

  public selected: boolean;
  public services: string[]
  id = uuid();

  constructor(public name:string, public imageUrl:string = "", public description:string = "", public votes: number = 0) {
    this.services = ['piscina', 'desayuno'];
  }

  public isSelected() : boolean {
    return this.selected;
  }

  public setSelected(selected: boolean) {
    this.selected = selected;
  }

  public voteUp() {
    this.votes++;
  }

  public voteDown() {
    this.votes--;
  }
}

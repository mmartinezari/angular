import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { FavoriteTripAction, NewTripAction } from './trip-state.model';
import { Trip } from './trip.model';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http'

@Injectable()
export class TripApiClient {
  trips: Trip[] = [];

  constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
    this.store
      .select(state => state.trips)
      .subscribe((data) => {
        console.log('Trips Sub Store');
        console.log(data);
        this.trips = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('All Store');
        console.log(data);
      });
  }

  add(t:Trip) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'tokeb-security'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/trips', { item: t.name }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status == 200) {
        this.store.dispatch(new NewTripAction(t));
        const myDd = db;
        myDd.trips.add(t);
        console.log('Todos los viajes de la Bdd!');
        myDd.trips.toArray().then(trips => console.log(trips));
      }
    });
  }

  choose(t: Trip) {
    this.store.dispatch(new FavoriteTripAction(t));
  }

  getById(id: string): Trip {
    return this.trips.filter(function(t) { return t.id.toString() === id; })[0];
  }

  getAll(): Trip[] {
    return this.trips;
  }
}

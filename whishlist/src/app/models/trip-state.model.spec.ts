import { reducerTrips, TripState, initializeTripState, InitTripAction, NewTripAction } from './trip-state.model';
import { Trip } from './trip.model';

describe('reducerTrips', () => {
  it('Should reduce init data', () => {
    //Setup
    const prevState: TripState = initializeTripState();
    const action: InitTripAction = new InitTripAction(['Barcelona', 'Tarragona']);
    //Action
    const newState: TripState = reducerTrips(prevState, action);
    //Assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].name).toEqual('Barcelona');
  });

  it('Should reduce new item added', () => {
    //Setup
    const prevState: TripState = initializeTripState();
    const action: NewTripAction = new NewTripAction(new Trip('Barcelona'));
    //Action
    const newState: TripState = reducerTrips(prevState, action);
    //Assertions
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].name).toEqual('Barcelona');
  });
});

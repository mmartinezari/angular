import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTracker]'
})
export class TrackerDirective {
  private element: HTMLInputElement;

  constructor(private elem: ElementRef) {
    this.element = elem.nativeElement;
    fromEvent(this.element, 'click').subscribe(event => this.track(event));
  }

  track(event: Event) : void {
    const tags = this.element.attributes.getNamedItem('data-tracker-tags').value.split(' ');
    console.log(`******* Track Event ${tags} *******`);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TripApiClient } from 'src/app/models/trip-api-client.model';
import { Trip } from 'src/app/models/trip.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  providers: [ TripApiClient ]
})
export class DetailComponent implements OnInit {
  trip: Trip;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: '/assets/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      id: 'countries',
      type: 'fill',
      source: 'world',
      layout: {},
      paint: {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private api: TripApiClient) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.trip = this.api.getById(id);
  }

}

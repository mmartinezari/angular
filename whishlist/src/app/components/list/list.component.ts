import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { TripApiClient } from '../../models/trip-api-client.model';
import { FavoriteTripAction, NewTripAction } from '../../models/trip-state.model';
import { Trip } from '../../models/trip.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ TripApiClient ]
})
export class ListComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Trip>;
  updates: string[];
  all;

  constructor(public api:TripApiClient, private store:Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit(): void {
    this.store.select(state => state.trips.favorite).subscribe(data => {
      if (data != null) {
        this.updates.push("Se ha elegido a " + data.name)
      }
    });
    this.store.select(state => state.trips.items).subscribe(items => this.all = items);
  }

  add(trip: Trip) {
    this.api.add(trip);
    this.onItemAdded.emit(trip);
  }

  select(trip: Trip) {
    this.api.choose(trip);
  }
}

import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { Trip } from '../../models/trip.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Trip>;
  fg: FormGroup;
  minLength: number = 20;
  data: string[] = ['Barcelona', 'Tarragona', 'Lleida', 'Girona', 'Torredembarra', 'Sabadell', 'Terrassa', 'Mollet', 'Hospitalet', 'Mataró' ];
  suggestions: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      name: ['', Validators.compose([
        Validators.required,
        this.invalidValidator
      ])],
      url: [''],
      description: ['', Validators.compose([
        Validators.required,
        this.lengthValidator(this.minLength)
      ])]
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log("Cambio en el formulario: ", form)
    });
  }

  ngOnInit(): void {
    let elem = <HTMLInputElement>document.getElementById('name');
    fromEvent(elem, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length >= 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/cities?q=' + text))
    ).subscribe(ajaxResponse => this.suggestions = ajaxResponse.response);

    //   switchMap(() => ajax('/assets/datos.json')),
    // ).subscribe(ajaxResponse => {
    //   this.suggestions = ajaxResponse.response
    //     .filter(function(x) {
    //       return x.toLowerCase().includes(elem.value.toLowerCase());
    //     });
    // });
  }

  save(name: string, url: string, description: string) : boolean {
    let trip = new Trip(name, url, description);
    this.onItemAdded.emit(trip);
    return false;
  }

  invalidValidator(control: FormControl) : { [s: string]: boolean } {
    console.log('invalidNameValidator');
    const length = control.value.toString().trim().length;
    if (length > 0 && length < 5) {
      return { "invalid": true };
    }
    return null;
  }

  lengthValidator(minLength: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const length = control.value.toString().trim().length;
      if (length > 0 && length < minLength) {
        return { "length": true };
      }
      return null;
    }
  }
}

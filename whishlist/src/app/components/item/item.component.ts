import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpTripAction, VoteDownTripAction } from '../../models/trip-state.model';
import { Trip } from '../../models/trip.model';
import { trigger, state, style, transition, animate } from '@angular/animations'

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  animations: [
    trigger('isFavorite', [
      state('stateFavorite', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('stateNoFavorite', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('stateNoFavorite => stateFavorite', [
        animate('3s')
      ]),
      transition('stateFavorite => stateNoFavorite', [
        animate('1s')
      ])
    ])
  ]
})
export class ItemComponent implements OnInit {
  @Input() trip: Trip;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Trip>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  visit() {
    this.clicked.emit(this.trip);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpTripAction(this.trip));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownTripAction(this.trip));
    return false;
  }
}

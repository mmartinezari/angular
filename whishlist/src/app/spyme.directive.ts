import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appSpyme]'
})
export class SpymeDirective implements OnInit, OnDestroy {
  static nextId = 0;
  log = (msg: string) => console.log(`Evento #${SpymeDirective.nextId++} ${msg}`);
  ngOnInit(): void { this.log('******** onInit ********'); }
  ngOnDestroy(): void { this.log('******* onDestroy *******') }
}

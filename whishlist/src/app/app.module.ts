import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './components/item/item.component';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { FormComponent } from './components/form/form.component';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';

import { initializeTripState, InitTripAction, reducerTrips, TripEffects, TripState } from './models/trip-state.model';
import { AuthService } from './services/auth.service';
import { UserGuard } from './guards/user/user.guard';
import { ItemComponent as FlightComponent } from './components/flights/item/item.component';
import { MainComponent as FlightMainComponent } from './components/flights/main/main.component';
import { InfoComponent as FlightInfoComponent } from './components/flights/info/info.component';
import { DetailComponent as FlightDetailComponent } from './components/flights/detail/detail.component';
import { BookingModule } from './booking/booking.module';
import { Trip } from './models/trip.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { SpymeDirective } from './spyme.directive';
import { TrackerDirective } from './tracker.directive';

export interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// Redux Init
export interface AppState {
  trips: TripState;
};

const reducers: ActionReducerMap<AppState> = {
  trips: reducerTrips
};

const reducersInitialState = {
  trips: initializeTripState()
};

// App Init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeTripState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}

  async initializeTripState() : Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'tokeb-security'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/trips', { headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitTripAction(response.body));
  }
}

//DexieDB
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  trips: Dexie.Table<Trip, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      trips: '++id, name, description, votes'
    });
    this.version(2).stores({
      trips: '++id, name, description, votes',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();

//i18n
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations.where('lang').equals(lang).toArray().then(results => {
      if (results.length === 0) {
        return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/translation?lang=' + lang)
          .toPromise()
          .then(apiResults => {
            db.translations.bulkAdd(apiResults);
            return apiResults;
          });
      }
      return results;
    }).then((translations) => {
      console.log('Traducciones cargadas...');
      console.log(translations);
      return translations;
    }).then((translations) => {
      return translations.map((t) => ({ [t.key]: t.value}));
    });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ListComponent,
    DetailComponent,
    FormComponent,
    LoginComponent,
    ProtectedComponent,
    FlightComponent,
    FlightMainComponent,
    FlightInfoComponent,
    FlightDetailComponent,
    SpymeDirective,
    TrackerDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([TripEffects]),
    StoreDevtoolsModule.instrument(),
    BookingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    UserGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

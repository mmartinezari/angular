import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { UserGuard } from './guards/user/user.guard';
import { MainComponent as FlightMainComponent } from './components/flights/main/main.component';
import { InfoComponent as FlightInfoComponent } from './components/flights/info/info.component';
import { DetailComponent as FlightDetailComponent } from './components/flights/detail/detail.component';
import { ItemComponent as FlightComponent } from './components/flights/item/item.component';

const childRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full'},
  { path: 'main', component: FlightMainComponent},
  { path: 'info', component: FlightInfoComponent},
  { path: ':id', component: FlightDetailComponent},
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListComponent},
  { path: 'trip/:id', component: DetailComponent},
  { path: 'login', component: LoginComponent},
  { path: 'protected', component: ProtectedComponent, canActivate: [ UserGuard ] },
  { path: 'flights', component: FlightComponent, canActivate: [ UserGuard ], children: childRoutes }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

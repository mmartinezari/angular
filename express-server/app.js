var express = require('express'), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var cities = [ "Paris", "Barcelona", "Londres", "Berlín", "Buenos Aires", "São Paulo", "New York" ];
app.get("/cities", (request, response, next) => {
    if (request.query.q == null) request.query.q = "";
    response.json(cities.filter((c) => c.toLowerCase().indexOf(request.query.q.toString().toLowerCase()) > -1));   
});

var trips = [];
app.get("/trips", (request, response, next) =>
    response.json(trips));

app.post("/trips", (request, response, next) => {
    console.log(request.body);
    trips.push(request.body.item);
    response.json(trips);
});

app.get("/translation", (request, response, next) => response.json([
    { lang: request.query.lang, key: 'HOLA', value: 'HOLA ' + request.query.lang }
]));